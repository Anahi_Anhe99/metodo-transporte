﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace programacion_lineal.Entidades
{
	public class Matriz_Costos
	{
		public double[,] MatrizCostos { get; set; }
		public double[] Oferta { get; set; }
		public double[] Demanda { get; set; }

	}
}
