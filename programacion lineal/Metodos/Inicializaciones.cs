﻿using programacion_lineal.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace programacion_lineal.Metodos
{
	public class Inicializaciones
	{
		List<double> penalizaciones_Oferta;
		List<double> penalizaciones_Demanda;
		public Inicializaciones()
		{
			penalizaciones_Oferta = new List<double>();
			penalizaciones_Demanda = new List<double>();
		}
		public double EsquinaNoroeste()
		{
			return 2.1;
		}
		public double CostoMinimo(Matriz_Costos matrizInicial)
		{
			double costMinimo = matrizInicial.MatrizCostos[0, 0];
			for (int i = 0; i < matrizInicial.Oferta.Length; i++)
			{
				for (int j = 0; j < matrizInicial.Demanda.Length; j++)
				{
					costMinimo = matrizInicial.MatrizCostos[i, j] < costMinimo ? matrizInicial.MatrizCostos[i, j] : costMinimo;
				}
			}
			return costMinimo;
		}

		public void Voguel(Matriz_Costos matrizInicial)
		{
			penalizaciones_Oferta.Clear();
			penalizaciones_Demanda.Clear();
			penalizaciones_Oferta = ObtenerPenalizaciones_Oferta(matrizInicial);
			penalizaciones_Demanda = ObtenerPenalizaciones_Demanda(matrizInicial);
			double pen_Maxima;
			pen_Maxima = ObtenerPenalizacionMayor(penalizaciones_Oferta, penalizaciones_Demanda);
		}

		private double ObtenerPenalizacionMayor(List<double> penalizaciones_Oferta, List<double> penalizaciones_Demanda)
		{
			return penalizaciones_Oferta.Max() > penalizaciones_Demanda.Max() ? penalizaciones_Oferta.Max() : penalizaciones_Demanda.Max();
		}

		#region Penalizaciones Oferta/Demanda
		private List<double> ObtenerPenalizaciones_Demanda(Matriz_Costos matrizInicial)
		{
			for (int i = 0; i < matrizInicial.Demanda.Length; i++)
			{
				List<double> columna = new List<double>();
				List<double> minimos = new List<double>();
				for (int j = 0; j < matrizInicial.Oferta.Length; j++)
				{
					columna.Add(matrizInicial.MatrizCostos[j, i]);
				}
				minimos = columna.Distinct().OrderBy(p => p).Take(2).ToList();
				if (minimos.Count == 1)
				{
					penalizaciones_Demanda.Add(0);
				}
				else
				{
					penalizaciones_Demanda.Add(Math.Abs(minimos[0] - minimos[1]));
				}
			}
			return penalizaciones_Demanda;
		}

		private List<double> ObtenerPenalizaciones_Oferta(Matriz_Costos matrizInicial)
		{
			for (int i = 0; i < matrizInicial.Oferta.Length; i++)
			{
				List<double> fila = new List<double>();
				List<double> minimos = new List<double>();
				for (int j = 0; j < matrizInicial.Demanda.Length; j++)
				{
					fila.Add(matrizInicial.MatrizCostos[i, j]);
				}
				minimos = fila.Distinct().OrderBy(p => p).Take(2).ToList();
				if (minimos.Count == 1)
				{
					penalizaciones_Oferta.Add(0);
				}
				else
				{
					penalizaciones_Oferta.Add(Math.Abs(minimos[0] - minimos[1]));
				}
			}
			return penalizaciones_Oferta;
		} 
		#endregion
	}
}
