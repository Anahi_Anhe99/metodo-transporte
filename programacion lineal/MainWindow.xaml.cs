﻿using programacion_lineal.Entidades;
using programacion_lineal.Metodos;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace programacion_lineal
{
	/// <summary>
	/// Lógica de interacción para MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		Matriz_Costos MatrizInicial;
		Inicializaciones operaciones;
		public MainWindow()
		{
			InitializeComponent();
			MatrizInicial = new Matriz_Costos();
			operaciones = new Inicializaciones();
		}
		int cantidadOferta = 0;
		int cantidadDemanda = 0;

		private void btnGenerarMatriz_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				cantidadOferta = int.Parse(txbNumOfertas.Text);
				cantidadDemanda = int.Parse(txbNumDemandas.Text);
				if (cantidadOferta > 0 && cantidadDemanda > 0)
				{
					stkMatrizCostos.Children.Clear();
					stkDemandas.Children.Clear();
					stkOfertas.Children.Clear();
					for (int i = 0; i < cantidadOferta; i++)
					{
						StackPanel Fila = new StackPanel();
						Fila.Orientation = Orientation.Horizontal;
						for (int j = 0; j < cantidadDemanda; j++)
						{		 
							Fila.Children.Add(CrearElemento(0) as TextBox);
							if (i==0)
								stkDemandas.Children.Add(CrearElemento(1) as TextBox);
						}
						stkMatrizCostos.Children.Add(Fila);
						stkOfertas.Children.Add(CrearElemento(2) as TextBox);
					} 

				}
				else
				{
					MessageBox.Show("Amiguito, asegurate de que hayas ingresado valores positivos :c", "Atención", MessageBoxButton.OK, MessageBoxImage.Error);
					txbNumDemandas.Clear();
					txbNumOfertas.Clear();
				}
			}
			catch (Exception)
			{
				MessageBox.Show("Amiguito, asegurate de haber ingresado el formato correcto", "Atención!", MessageBoxButton.OK, MessageBoxImage.Error);
				txbNumOfertas.Clear();
				txbNumDemandas.Clear();
			}
		}


		private void btnEsquinaNoroeste_Click(object sender, RoutedEventArgs e)
		{
			AsignarMatrizCostos();
		}
		private void btnCostoMinimo_Click(object sender, RoutedEventArgs e)
		{
			AsignarMatrizCostos();
			double costoMinimo = operaciones.CostoMinimo(MatrizInicial);
		}

		private void btnVoguel_Click(object sender, RoutedEventArgs e)
		{
			AsignarMatrizCostos();
			operaciones.Voguel(MatrizInicial);
		}

		#region Crear Celdas
		private TextBox CrearElemento(int v)
		{
			TextBox elemento = new TextBox()
			{
				Height = 23,
				Width = 41,
				Margin = new Thickness(5)
			};
			elemento.ToolTip = v == 1 ? "Cantidad de Demanda" : v == 2 ? "Cantidad de Oferta" : "Costo";
			elemento.Text = v == 1 ? "0" : v == 2 ? "0" : "";
			elemento.Background = v == 1 ? Brushes.LightGreen : v == 2 ? Brushes.LightSkyBlue : Brushes.Transparent;
			return elemento;
		}
		#endregion

		#region Balancear TablaIncializacion

		private void BalancearProblema()
		{
			float ofertaTotal = 0;
			float demandaTotal = 0;
			foreach (var elemento in stkOfertas.Children)
			{
				TextBox _elemento = elemento as TextBox;
				ofertaTotal += float.Parse(_elemento.Text);
			}
			foreach (var elemento in stkDemandas.Children)
			{
				TextBox _elemento = elemento as TextBox;
				demandaTotal += float.Parse(_elemento.Text);
			}
			if (ofertaTotal != demandaTotal)
			{
				if (demandaTotal > ofertaTotal)
				{
					StackPanel stackPanel = new StackPanel();
					stackPanel.Orientation = Orientation.Horizontal;
					for (int i = 0; i < cantidadDemanda; i++)
					{
						TextBox elemento = CrearElemento(0);
						elemento.Text = "0";
						elemento.IsEnabled = false;
						stackPanel.Children.Add(elemento);
					}
					stkMatrizCostos.Children.Add(stackPanel);
					TextBox Ficticio = CrearElemento(2);
					Ficticio.Text = (demandaTotal - ofertaTotal).ToString();
					stkOfertas.Children.Add(Ficticio);
					cantidadOferta++;  
				}
				else
				{
					foreach (var fila in stkMatrizCostos.Children)
					{
						StackPanel stkfila = fila as StackPanel;
						TextBox elemento = CrearElemento(0);
						elemento.Text = "0";
						elemento.IsEnabled = false;
						stkfila.Children.Add(elemento);
					}
					TextBox Ficticcio = CrearElemento(1);
					Ficticcio.Text = (ofertaTotal - demandaTotal).ToString();
					stkDemandas.Children.Add(Ficticcio);
					cantidadDemanda++;
				}
				
			}
			else
			{
				MessageBox.Show("Problema balanceado", "Atención", MessageBoxButton.OK, MessageBoxImage.Information);
			}
			MatrizInicial.Demanda = new double[cantidadDemanda];

			for (int i = 0; i < cantidadDemanda; i++)
			{
				MatrizInicial.Demanda[i] = double.Parse((stkDemandas.Children[i] as TextBox).Text);
			}
			MatrizInicial.Oferta = new double[cantidadOferta];

			for (int i = 0; i < cantidadOferta; i++)
			{
				MatrizInicial.Oferta[i] = double.Parse((stkOfertas.Children[i] as TextBox).Text);
			}
		}
		#endregion

		public void AsignarMatrizCostos()
		{
			int i = 0;
			int j = 0;
			MatrizInicial.MatrizCostos = new double[cantidadOferta, cantidadDemanda];
			foreach (var fila in stkMatrizCostos.Children)
			{
				j = 0;
				StackPanel _fila = fila as StackPanel;
				foreach (var celda in _fila.Children)
				{
					TextBox _celda = celda as TextBox;
					if (_celda.Text == "M"|| _celda.Text == "M")
					{
						MatrizInicial.MatrizCostos[i, j] = 100000;
					}
					else
					{
						MatrizInicial.MatrizCostos[i, j] = double.Parse(_celda.Text);
					}
					if (cantidadDemanda - 1 != j)
						j++;

				}
				if (cantidadOferta - 1 != i)
					i++;
			}
		}

		private void btnBalancearTabla_Click(object sender, RoutedEventArgs e)
		{
			BalancearProblema();
			
		}
	}
}
